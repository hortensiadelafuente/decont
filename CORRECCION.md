### download.sh

- [-1] No se utilizan los argumentos como indican las instrucciones
- [-1] El script no es genérico y reutilizable, sino que descarga los datos específicos del ejercicio

### index.sh 

- [-1] No se utilizan los argumentos como indican las instrucciones

### merge_fastqs.sh

- [-0.5] Se utilizan patrones específicos para dos archivos diferentes en lugar de sólo el sample id ($1/$sid*.fastq.gz)

### pipeline.sh

- [-0.5] El log final no especifica a qué muestra corresponden los datos de cutadapt
