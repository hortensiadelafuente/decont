# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".


mkdir -p res/contaminant
mkdir -p res/contaminant/contaminants_idx
mkdir -p out/merged
mkdir -p out/trimmed
mkdir -p log/cutadapt
mdkir -p out/star

cd data
wget -i urls 

echo
gunzip -k *.fastq.gz

echo
cd ..

cd res/contaminant
wget  https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz

gunzip -k contaminants.fasta.gz

cd ..
cd ..

echo


