
#Download all files from data to be analyzed and the contaminants, and uncompress it

bash scripts/download.sh 



# Index the contaminants file
bash scripts/index.sh 


# Merge the samples into a single file
echo "Running merge" 

for sid in $(ls data/*.fastq.gz | cut -d "." -f1 | sed 's:data/::' | sort | uniq) 
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done

# TODO: run cutadapt for all merged files
echo "running cutadapt" 

for sampleid in $(ls out/merged | cut -d "." -f1 | sed 's:out/merged/::' | sort | uniq)
do
cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sampleid}.trimmed.fastq.gz  out/merged/${sampleid}.merged.fastq.gz > log/cutadapt/${sampleid}.log
done

#TODO: run STAR for all trimmed files
echo "Running STAR alignment"

for fname in $(ls out/trimmed | cut -d "." -f1 | sed 's:out/trimmed/::' | sort |uniq)
do
mkdir -p out/star/${fname}

STAR --runThreadN 4 --genomeDir res/contaminant/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/${fname}.trimmed.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix out/star/${fname}/
done
    
cat log/cutadapt/*.log | grep -e "Reads with adapters" -e "Total basepairs processed" >> log/resultados.log

for results in $(ls out/star)
do
echo $results >> log/resultados.log
cat out/star/$results/Log.final.out | grep -e '% of reads mapped to too many loci' -e 'Uniquely mapped reads %' -e '% of reads mapped to multiple loci' >> log/resultados.log
done
# TODO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run)
# - cutadapt: Reads with adapters and total basepairs
# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in
