# This script should merge all files from a given sample (the sample id is provided in the third argument ($3))
# into a single file, which should be stored in the output directory specified by the second argument ($2).
# The directory containing the samples is indicated by the first argument ($1).

 
sid=$3
data=$1


echo "merge fastqs.."

cat $1/${sid}*1.1*gz $1/${sid}*1.2*gz  > out/merged/${sid}.merged.fastq.gz

echo

